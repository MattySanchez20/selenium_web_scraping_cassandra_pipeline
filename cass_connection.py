# webscraping using selenium
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

# for data cleaning
import re
import datetime

# driver to connect to cassandra database
from cassandra.cluster import Cluster

# options allows you to set Chrome browser options
options = Options()

# detach means it allows chrome to run in a seperate window (set to true)
options.add_experimental_option('detach', True)

# driver uses google chrome as the web browser
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()),
                        options=options)

# directing the driver to the desired url
driver.get('https://www.rightmove.co.uk/house-prices/york.html?page=1')

# setting string to obtain html of website with class = "pagination-label" to obtain last page number
search_string = '//span[contains(@class, "pagination-label")]'

# obtaining html using string specified above
dvr = driver.find_elements('xpath', search_string)

# obtains the last pagination number to know how many pages to iterate through
for d in dvr:
    for valu in d.get_attribute('innerHTML').strip().split():
        if valu.isalnum():
            page_max = valu

# search string to obtain html with class = "propertyCard-content"
search = '//body//div[contains(@class, "propertyCard-content")]'

# creating empty list to store details obtained from the web scraping
house_deets = []
    
# loops over all pages and obtains house details from html box using search string above
for i in range(int(page_max)+1):
    driver.get(f'https://www.rightmove.co.uk/house-prices/york.html?page={i}')

    dvr = driver.find_elements('xpath', search)

    for d in dvr:
        house_deets.append(d.text.strip())
    
# closes chrome session and driver
driver.close()


# strings containing regex to obtain certain details from text obtained from web scrape

# test string
string = 'here 49, Russell Street, York YO23 1NN 2 bed, terraced £360,000 10 Oct 2022 Freehold £210,000 15 Jan 2010 Freehold £74,950 23 Jul 1999 Freehold'

# obtains price of house by looking for number after pound sign
price_regex = '£([0-9]*,...)'

# obtains postcode by looking for words that are characteristic of a York postcode e.g., starts with YO
postcode_regex = '(YO[0-9]* [0-9][A-Z]*)'

# obtains date with format DD ShortMonth Year for example: 12 Jan 2012
date_regex = '([1-9]?[0-9]) ([A-Z][a-z]+) ([19]?[20]?[0-9][0-9][0-9])'

# obtains street name by locating position of street name within the whole string
street_regex = ', ([A-Z][a-z]*\ ?[A-Z]?[a-z]*?\ ?[A-Z]?[a-z]*?),'

# obtains number of beds by looking for number infront of the word 'bed'
beds_regex = '([1-9]?[0-9]) bed'

# connecting to clustesr and starting a session to intercact with the database using python keyspace
cluster = Cluster(['host.docker.internal'], port=9042)
session = cluster.connect('python')

# cassandra query to create a table if it doesn't exist
create_table_query = """
    CREATE TABLE IF NOT EXISTS housing (
        id uuid PRIMARY KEY,
        postcode text,
        street text,
        bed_num int,
        price int,
        listing_date date,
    )
"""

# execute query above
session.execute(create_table_query)

for deets in house_deets:

    # use of regular expressions to obtain house details
    postcode = re.findall(postcode_regex, deets)
    dates = re.findall(date_regex, deets)
    prices = re.findall(price_regex , deets)
    street = re.findall(street_regex, deets)
    bed = re.findall(beds_regex, deets)
    
    # conditional to take first value of bed list if bed is not a list
    if len(bed) == 0:
        bed = [0]
    
    # looping through the length of the longest list (prices list)
    for i in range(len(prices)):
        
        # removes comma from price string
        p1, p2 = prices[i].split(',')
        combined_price = p1 + p2
        
        # changes data type of date string from object to datetime then cleaning the string
        date = str(datetime.datetime.strptime(f'{dates[i][0]} {dates[i][1]} {dates[i][2]}', '%d %b %Y'))
        date = date.split(' ')

        # query to insert values into a table created above. uuid() is an automatic unique id
        insert_query = """
            INSERT INTO housing (id, postcode, street, bed_num, price, listing_date)
            VALUES (uuid(), %s, %s, %s, %s, %s)
        """

        # executing query above and specifying values to insert into table
        session.execute(insert_query, (postcode[0], street[0], int(bed[0]), int(combined_price), f'{date[0]}'))


# shutting down session and cluster connection
session.shutdown()
cluster.shutdown()
